/* 3. Add a container in the center of the screen with a size(width: 200,
height: 200). Give a red border to the container. Now when the user taps
the container change the color of the border to green.*/
import 'package:flutter/material.dart';
class Problem3 extends StatefulWidget{ 
  const Problem3({super.key});
  @override 
  State<Problem3>createState()=>_Problem3State();
  }
class _Problem3State extends State<Problem3>{
  bool isColorChange =true;
  @override 
  Widget build (BuildContext context){
    return Scaffold( 
      body: Center( 
        child:GestureDetector(
          onTap: (){ 
            setState(() {
              isColorChange=!isColorChange;
            });
          },
          child: Container( 
            height: 200,
            width: 200, 
            decoration:BoxDecoration( 
              border:Border.all(
                width: 5,
                color:(isColorChange)?Colors.red:Colors.blue)
            ) ,
          ),
        ),
      ),
    );
  }
}