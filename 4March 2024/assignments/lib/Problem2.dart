/*Create a Container in the Center of the screen, now In the background of
the Container display an Image (the image can be an asset or network
image ). Also, display text in the center of the Container.*/
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Problem2 extends StatelessWidget{ 
  const Problem2({super.key});

  @override 
  Widget build(BuildContext context){ 
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue, 
        title: const Text("Container",
        style: TextStyle( 
          color: Colors.white,
        ),
        ),
      ),
      body: Center( 
        child:Container(alignment: Alignment.center,
          padding: const EdgeInsets.all(10), 
          height: 300,
          width: 300,
          decoration: const BoxDecoration( 
            image:DecorationImage(image: NetworkImage("https://as1.ftcdn.net/v2/jpg/03/08/92/74/1000_F_308927436_0hJRZ9DcpV55OH5I9jWaB2sIuWefJwWA.jpg",
          
            ),
            fit:BoxFit.cover,
          ),
          
          ),
          child: const Text("Welcome", 
          style: TextStyle( 
            fontSize: 25,
            color: Colors.pinkAccent,
            fontWeight: FontWeight.w700,
          ),
          ),
          
        ),
      ),
    );
  }
}