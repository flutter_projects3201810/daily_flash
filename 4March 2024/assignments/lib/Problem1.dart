/*Create a Container in the Center of the Screen with size(width: 300,
height: 300) and display an image in the center of the Container. Apply
appropriate padding to the container.*/
import 'package:flutter/material.dart';

class Problem1 extends StatelessWidget{ 
  const Problem1({super.key});

  @override 
  Widget build(BuildContext context){ 
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue, 
        title: const Text("Container",
        style: TextStyle( 
          color: Colors.white,
        ),
        ),
      ),
      body: Center( 
        child:Container(
          color: Colors.blue,
          padding: const EdgeInsets.all(10), 
          height: 300,
          width: 300,
          child: Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQHM8nYvCNVEv9Zb2z1NeTNCzoj3n27wq4LY8C5R3nFFA&s",
          fit:BoxFit.cover,
          ),
        )
      ),
    );
  }
}