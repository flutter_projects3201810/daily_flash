/* 5. Create a Circular Container and give the Container 2 colors i.e. red and
blue. 50 % of the container must contain red and the other 50 % must
contain blue color.
(Note: The transition from the Red color to blue must be sharp)*/

import 'package:flutter/material.dart';

class Problem5 extends StatelessWidget{ 
  const Problem5({super.key});

  @override 
  Widget build(BuildContext context){ 
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue, 
        title: const Text("Container",
        style: TextStyle( 
          color: Colors.white,
        ),
        ),
      ),
      body: Center( 
        child:Container(
          
          height:200,
          width: 200,
          decoration: const BoxDecoration( 
            gradient:LinearGradient(
              stops: [0.5,0.5],
              colors: [
              
              Colors.red,Colors.blue
            ]),
            shape: BoxShape.circle,
          ),
          ),
        )
      
    );
  }
}