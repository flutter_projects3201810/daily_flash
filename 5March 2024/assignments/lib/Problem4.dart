/* 4. Create a Container with size(height:200, width:300) now give a shadow to
the container but the shadow must only be at the top side of the container.*/
/*Create a Container in the Center of the Screen with size(width: 300,
height: 300) and display an image in the center of the Container. Apply
appropriate padding to the container.*/
import 'package:flutter/material.dart';

class Problem4 extends StatelessWidget{ 
  const Problem4({super.key});

  @override 
  Widget build(BuildContext context){ 
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue, 
        title: const Text("Container",
        style: TextStyle( 
          color: Colors.white,
        ),
        ),
      ),
      body: Center( 
        child:Container(
          height: 200,
          width: 200,
          decoration:const BoxDecoration( 
            color: Colors.blue,
            boxShadow:[ 
              BoxShadow( 
                blurRadius: 10,
              offset: Offset(0,-10),
            
              color: Colors.black,
              ),
            ],
          ),
          ),
        )
      
    );
  }
}