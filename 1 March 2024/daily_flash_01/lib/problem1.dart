/*1. Create an AppBar, give an Icon at the start of the appbar, give a title
in the middle, and at the end add an Icon.*/
import 'package:flutter/material.dart';

class Problem1 extends StatelessWidget{
  const Problem1({super.key});

  @override 
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: const Icon(Icons.home),
          actions: const [
            Padding(
              padding: EdgeInsets.only(
                right: 15,
              ),
              child: Icon(Icons.menu),
            ),
          ],
          backgroundColor: const Color.fromARGB(255, 224, 90, 135),
          centerTitle: true,
          title: const Text(
            "AppBar",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w700,
              color: Colors.white,
            ),
          ),
        ),
      );

  }
}