/*2. Create an AppBar give a color of your choice to the AppBar and then
add an icon at the start of the AppBar and 3 icons at the end of the
AppBar.*/
import 'package:flutter/material.dart';



class Problem2 extends StatelessWidget {
  const Problem2({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          leading: const Icon(Icons.home),
          actions: const [
            Icon(Icons.menu),
            Icon(Icons.notifications),
            Icon(Icons.message),

          ],
          backgroundColor: const Color.fromARGB(255, 224, 90, 135),
          centerTitle: true,
          title: const Text(
            "AppBar",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w700,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
