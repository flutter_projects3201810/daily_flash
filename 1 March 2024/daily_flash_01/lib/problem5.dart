/*5. Create a Screen, in the center of the Screen display a Container with
rounded corners, give a specific color to the Container, the container
must have a shadow of color red. */
import 'package:flutter/material.dart';

class Problem5 extends StatelessWidget {
  const Problem5({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 232, 129, 163),
        centerTitle: true,
        title: const Text(
          "Container",
          style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.w700,
            color: Colors.white,
          ),
        ),
      ),
      body: Center( 
        child: Container( 
          height: 300,
          width: 300,
          
          decoration: BoxDecoration(
            
            color: const  Color.fromARGB(255, 105, 168, 220),
            borderRadius: BorderRadius.circular(20),
            boxShadow:const [
              BoxShadow( 
                color: Colors.red,
                blurRadius: 5,
                spreadRadius: 5,
              ),
            ],

            ),
            

            //color:const Color.fromARGB(255, 185, 24, 12),
            
          ),
        ),
      
    );
  }
}
