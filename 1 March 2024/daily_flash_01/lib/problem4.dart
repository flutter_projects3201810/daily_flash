/* 4. Create a Screen that will display the Container in the Center of the
Screen,
with size(width: 300, height: 300). The container must have a blue
color and it must have a border which must be of color red.*/
import 'package:flutter/material.dart';

class Problem4 extends StatelessWidget {
  const Problem4({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 232, 129, 163),
        centerTitle: true,
        title: const Text(
          "Container",
          style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.w700,
            color: Colors.white,
          ),
        ),
      ),
      body: Center( 
        child: Container( 
          height: 300,
          width: 300,
          
          decoration: BoxDecoration( 
            color: const Color.fromARGB(255, 105, 168, 220),
            border: Border.all(
              width: 10,
            color:const Color.fromARGB(255, 185, 24, 12),
            ),
          ),
        ),
      ),
    );
  }
}
