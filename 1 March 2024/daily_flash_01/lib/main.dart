
import 'package:daily_flash_01/solution.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Problem5(),
      //Problem4(),
      //Problem3(),
      //Problem2(),
      //Problem1(),
    );
  }
}
