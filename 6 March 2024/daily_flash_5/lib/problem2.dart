/* 2. Create a Screen in which we have 3 Containers in a Column each container
must be of height 100 and width 100. Each container must have an image
as a child.*/
import 'package:flutter/material.dart';

class Problem2 extends StatelessWidget {
  const Problem2({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.greenAccent,
        centerTitle: true,
        title: const Text(
          "Profile Information",
          style: TextStyle(
            fontSize: 26,
            fontWeight: FontWeight.w700,
            color: Colors.white,
          ),
        ),
      ),
      body: Row(mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              const SizedBox( 
                height: 100,
              ),
              Container(
                height: 100,
                width: 100,
                child: Image.asset(
                  "images/deepika.jpg",
                  fit: BoxFit.cover,
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                height: 100,
                width: 100,
                child: Image.asset(
                  "images/dhoni.jpg",
                  fit: BoxFit.cover,
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                height: 100,
                width: 100,
                child: Image.asset(
                  "images/Kohali.jpg",
                  fit: BoxFit.cover,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
