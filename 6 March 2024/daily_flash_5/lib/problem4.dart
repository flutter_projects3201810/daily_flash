/* 4. Create a Screen in which we will display 3 Containers of Size 100,100 in a
Row. Give color to the containers. The containers must divide the free
space in the main axis evenly among each other.*/
import 'package:flutter/material.dart';

class Problem4 extends StatelessWidget {
  const Problem4({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        centerTitle: true,
        title: const Text(
          "Containers",
          style: TextStyle(
            fontSize: 26,
            fontWeight: FontWeight.w700,
            color: Colors.white,
          ),
        ),
      ),
      body:Center( 
        child: Row( mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container( 
              height: 100,
              width: 100,
              color: Colors.red,
            ),
            Container( 
              height: 100,
              width: 100,
              color: Colors.yellow,
            ),
            Container( 
              height: 100,
              width: 100,
              color: Colors.green,
            ),
          ],
          
        ),
      ),
       );
       }
}