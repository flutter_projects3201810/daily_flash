/*5. Create a Screen that displays 3 widgets in a Column. The image must be the
first widget, the next widget must be a Container of color red and the 3rd
widget must be a Container of color blue. Place all the 3 widgets in a
Column.
The Image must be placed at the top center and the other 2 widgets must
be placed at the bottom center of the screen. */
import 'package:flutter/material.dart';

class Problem5 extends StatelessWidget {
  const Problem5({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        centerTitle: true,
        title: const Text(
          "Containers",
          style: TextStyle(
            fontSize: 26,
            fontWeight: FontWeight.w700,
            color: Colors.white,
          ),
        ),
      ),
      body:Center( 
        child: Column( mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container( 
              height: 100,
              width: 100,
              
              child:Image.asset("images/deepika.jpg",
              fit: BoxFit.cover,
              ),
            ),
            Container( 
              height: 100,
              width: 100,
              color: Colors.red,
            ),
            Container( 
              height: 100,
              width: 100,
              color: Colors.green,
            ),
          ],
          
        ),
      ),
       );
       }
}