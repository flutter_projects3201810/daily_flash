/*1. Create a Screen, in the appBar display "Profile Information". In the body,
display an image of size (height: 250 width:250). Below the image add
appropriate spacing and then display the user Name and Phone Number
vertically. The name and phone number must have a font size of 16 and a font
weight of 500.*/

import 'package:flutter/material.dart'; 

class Problem1 extends StatelessWidget {
  const Problem1({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( 
        backgroundColor: Colors.greenAccent,
        centerTitle: true,
        title: const Text("Profile Information",
        style: TextStyle( 
          fontSize:26,
          fontWeight:FontWeight.w700,
          color: Colors.white,
        ),
        ),
      ),
      body:Center(
        child: Column( mainAxisAlignment: MainAxisAlignment.center,
          children: [ 
            
            Image.asset("images/deepika.jpg",
            ),
            const SizedBox( 
              height: 30,
            ),
            const Text(" Username: Deepika Padukon",
            style:TextStyle( 
              fontSize: 16,
              fontWeight: FontWeight.w500,
            ) ,
            ),
            const SizedBox( 
              height: 30,
            ),
            const Text(" Phone Number: 8593192810",
            style:TextStyle( 
              fontSize: 16,
              fontWeight: FontWeight.w500,
            ) ,
            ),
          ],
        ),
      ),
    );
  }
}
