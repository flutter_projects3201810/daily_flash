/*3. Create a Screen and add your image in the center of the screen below your
image display your name in a container, give a shadow to the Container
and give a border to the container the top left and top right corners must
be circular, with a radius of 20. Add appropriate padding to the container.*/

import 'package:flutter/material.dart';

class Problem3 extends StatelessWidget {
  const Problem3({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        centerTitle: true,
        title: const Text(
          "Profile Information",
          style: TextStyle(
            fontSize: 26,
            fontWeight: FontWeight.w700,
            color: Colors.white,
          ),
        ),
      ),
      body: Center(

        child: Column(mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 400,
              width:400 ,
            child: Image.asset("images/aarti.jpeg"), 
            ),
            const SizedBox( 
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container( 
                alignment: Alignment.center,
              
                height: 50,
                width: 330,
                decoration:  BoxDecoration( 
                  color: Colors.blue,
                  border: Border.all( 
                    width: 5,
                    color:const  Color.fromARGB(255, 12, 73, 123),
                  ),
                  
                  borderRadius:const  BorderRadius.only( 
                    
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)
              
                  ),
                ),
                child: const Text(" Aarti",
                style: TextStyle( 
                  fontSize: 30,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
