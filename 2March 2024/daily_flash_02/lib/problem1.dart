/*1. Create a screen that displays the container in the center having size (height:
200, width: 200). The Container must have border with rounded edges. The
border must be of the color red. Display a Text in the center of the container. */
import 'package:flutter/material.dart';

class Problem1 extends StatelessWidget {
  const Problem1({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar( 
        backgroundColor:const Color.fromARGB(255, 103, 175, 234),
        centerTitle: true,
        title: const Text("Container",
        style: TextStyle( 
          fontSize:30,
          fontWeight:FontWeight.w700,
          color: Colors.white,
          
        ),
        ),
      ),
      body:Center( 
        child: Container( alignment: Alignment.center,
          height: 200,
          width: 200,
          decoration: BoxDecoration( 
          color: Colors.blue,
          border: Border.all(
            color: Colors.red,
            width: 5,
          ),
          ),
          child: const Text(" Hello",
          style: TextStyle( 
            fontSize: 25,
            fontWeight: FontWeight.w500,
          ),
          ),

          ),
        ),
      );
  }
}
