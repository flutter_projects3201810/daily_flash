/*4. Create a container that will have a border. The top right and bottom left corners
of the border must be rounded. Now display the Text in the Container and give
appropriate padding to the container. */
/*3. In the screen add a container of size( width 100, height: 100) . The container
must have a border as displayed in the below image. Give color to the container
and border as per your choice.*/
import'package:flutter/material.dart';
class Problem4 extends StatelessWidget{ 
  const Problem4({super.key});
  @override 
  Widget build(BuildContext context){ 
    return Scaffold( 
      appBar:AppBar( 
        backgroundColor:const Color.fromARGB(255, 236, 48, 110),
        title: const Text(" Container",
        style: TextStyle(
          fontSize:30,
          fontWeight: FontWeight.w700,
          color: Colors.white,
        ),),
      ),
      body: Center(
        child: Container(
          height: 100,
          width: 100,
          padding:const  EdgeInsets.all(10),
          decoration: BoxDecoration( 
            color:   const Color.fromARGB(255, 245, 200, 215),
            border: Border.all( 
              color: const Color.fromARGB(255, 236, 48, 110),
              width: 5,
            ),
            borderRadius: const BorderRadius.only( 
              topLeft:Radius.circular(20),
              bottomRight: Radius.circular(20),
              
            ),

            
          ),
          child: const Text("Hello",
          style: TextStyle(
            fontSize:15,
            fontWeight: FontWeight.bold,
            color:  Color.fromARGB(255, 236, 48, 110),
          ),
          ),
        ),
        ),
    );
  }
}