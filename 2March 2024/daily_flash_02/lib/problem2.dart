/* 2. In the screen add a container of size( width 100, height: 100) that must only
have a left border of width 5 and color as per your choice. Give padding to the
container and display a text in the Container.*/

import 'package:flutter/material.dart';

class Problem2 extends StatelessWidget{ 
  const Problem2({super.key});

  @override 
  Widget build(BuildContext context){ 
    return Scaffold(
      appBar: AppBar( 
        backgroundColor:Colors.blue,
        title:const Text("Container",
        style: TextStyle( 
          fontSize:30,
          fontWeight:FontWeight.w700,
          color:Colors.white,
        ),
        ),
      ),
      body:Center( 
        child: Container( 
          height: 100,
          width: 100,
          padding: const EdgeInsets.symmetric( 
          vertical: 10,
          horizontal: 10,
          ),
          decoration: const BoxDecoration( 
            color:Colors.blue,
            border:Border( 
              left: BorderSide(
                color: Colors.red,
                width: 5,
              ),
            ),

            
          ),
        child:const Text(" Hello",
        style: TextStyle( 
          fontSize: 25,
          fontWeight: FontWeight.w500,
          color: Colors.black,
        ),
        ),
        ),
      ),
    );
  }
}