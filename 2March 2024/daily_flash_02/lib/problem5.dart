/* 5. Add a container with the color red and display the text "Click me!" in the center
of the container. On tapping the container, the text must change to “Container
Tapped” and the color of the container must change to blue.*/

import'package:flutter/material.dart';

class Problem5 extends StatefulWidget{ 
  const Problem5({super.key});
  @override 
  State<Problem5> createState()=> _Problem5State();
}
class _Problem5State extends State<Problem5>{

  bool isColorChange =true;
@override 
Widget build(BuildContext context){
  return Scaffold( appBar:AppBar(
    backgroundColor: Colors.blue,
    centerTitle: true,
    title: const Text("Container",
    style: TextStyle( 
      fontSize:30,
      fontWeight:FontWeight.w700,
      color:Colors.white,
    ),
    ),
  ),
  body:Center( 
    child: GestureDetector(
      onTap: (){
        setState(() {
          isColorChange = !isColorChange;
        });
      },
      child: Container( alignment: Alignment.center,
        height: 300,
        width: 300,
        
        color: (isColorChange)?Colors.red:Colors.blue,
        child: (isColorChange)?const Text(" Click me",
        style: TextStyle( 
          fontSize: 30,
          fontWeight: FontWeight.bold,
        ),
        ):const Text("Container Tapped",
        style: TextStyle( 
          fontSize: 30,
          fontWeight: FontWeight.bold,
        ),
        ),
      ),
    ),
  ),
  );
}
}
