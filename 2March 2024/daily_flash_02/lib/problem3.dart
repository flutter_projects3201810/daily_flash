/*3. In the screen add a container of size( width 100, height: 100) . The container
must have a border as displayed in the below image. Give color to the container
and border as per your choice.*/
import'package:flutter/material.dart';
class Problem3 extends StatelessWidget{ 
  const Problem3({super.key});
  @override 
  Widget build(BuildContext context){ 
    return Scaffold( 
      appBar:AppBar( 
        backgroundColor:const  Color.fromARGB(255, 177, 111, 188),
        title: const Text(" Container",
        style: TextStyle(
          fontSize:30,
          fontWeight: FontWeight.w700,
          color: Colors.white,
        ),),
      ),
      body: Center(
        child: Container(
          height: 100,
          width: 100,
          decoration: BoxDecoration( 
            color:  const Color.fromARGB(255, 177, 111, 188),
            border: Border.all( 
              color: const Color.fromARGB(255, 80, 5, 93),
              width: 5,
            ),
            borderRadius: const BorderRadius.only( 
              topRight: Radius.circular(20,
              ),
              
            ),

            
          ),
        ),
        ),
    );
  }
}